### Quelques chiffres 

#### Combien de tableaux ont été extraits ?

1643

#### Combien de colonnes, de lignes, et de cellules sont présentes dans les tableaux extraits?

| Stats         | Lignes          | Colonnes       | Cellules       |
| :-----------: | :-------------: | :------------: | :------------: |
| minimum       | 1               | 1              | 1              |
| maximum       | 452             | 88             | 5424           |
| moyenne       | 21.33           | 9.45           | 203.67         |
| écart-type    | 32.5            | 7.3            | 334            |


#### Quels sont les noms de colonnes les plus fréquents?

5 plus fréquents

| Nom           | Nombre          |
| :-----------: | :-------------: |
| Absence de nom| 1045            |
| Memory        | 559             |
| Fillrate      | 275             |
| Trident       | 229             |
| WebKit        | 223             |

#### Qualité et faiblesse de votre extracteur

Qualités :

* cellules concaténées en horizontal
* Doublons noms dans header
* Plusieurs headers (en haut et en bas par exemple) sont regroupés



Faiblesses :

* 1ere ligne de taille inférieure au reste du tableau
* cellules concaténées en hauteur
* table avec header vertical pas toujours reconnu

#### Comparaison par rapport à un extracteur pandas

Nous n'avons pas traité ce point.

### Synthèse générale

Bien qu'ayant une bonne extraction des tableaux, ces tableaux sont malheureusement inutilisables avec R.  
