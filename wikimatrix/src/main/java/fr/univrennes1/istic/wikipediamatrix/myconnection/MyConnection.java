package fr.univrennes1.istic.wikipediamatrix.myconnection;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class MyConnection {

    public static Document get(String url) throws IOException {
        Connection connection = Jsoup.connect(url);
        Connection.Response response = connection.execute();

        Document doc = null;

        if (response.statusCode() == 200) {
            doc = connection.get();
        }

        return doc;
    }

}
