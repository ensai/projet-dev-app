package fr.univrennes1.istic.wikipediamatrix.csv;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVFormat;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvReader {

    public static List<Map<String, String>> read(String path) throws IOException {

        try (Reader reader = Files.newBufferedReader(Paths.get(path));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader()
                        .withIgnoreHeaderCase().withTrim().withQuote(null).withEscape('"')))
        {
            List<String> headerNames = csvParser.getHeaderNames();
            List<Map<String, String>> resu = new ArrayList<>();
            String value;
            Map<String, String> map;
            for (CSVRecord csvRecord : csvParser) {
                map = new HashMap<>();
                // Accessing values by Header names
                for (String column : headerNames) {
                    value = csvRecord.get(column);
                    map.put(column,value);
                }
                resu.add(map);
               }
            return resu;
        }
    }
}
