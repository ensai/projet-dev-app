package fr.univrennes1.istic.wikipediamatrix.csv;

import org.jsoup.select.Elements;



class Line {
    private String[] line;

    Line(Elements tds) {
        int size = 0;
        for (org.jsoup.nodes.Element td : tds) {
            if (td.attr("colspan").equals("")) {
                size++;
            } else {
                size = size + Integer.parseInt(td.attr("colspan"));
            }
        }
        line = new String[size];
        StringBuilder value;
        int number;
        int curs = 0;
        for (int i=0; i< tds.size();i++) {
            value = new StringBuilder();
            value.append(tds.get(i).text());
            number = 0;
            for (int j=0; j < tds.size();j++){
                try {
                    if (line[j].contains(value.toString())){
                        number++;
                    }
                } catch (NullPointerException ignored) {

                }

            }
            if (tds.get(i).attr("colspan").equals("")) {
                if (number != 0) {
                    value.append(".");
                    value.append(number);
                }
                line[curs] = value.toString();
                curs++;
            } else {
                for (int j=0; j < Integer.parseInt(tds.get(i).attr("colspan")); j++) {
                    if (number != 0) {
                        line[curs] = value.toString() + "." + number;
                    } else {
                        line[curs] = value.toString();
                    }
                    number++;
                    curs++;
                }
            }
        }
    }

    Line(int size, Elements tds) {
        line = new String[size];
        int i = 0;
        int j = 0;
        while (i<tds.size() && j<size) {
            if (tds.get(i).attr("colspan").equals("")) {
                line[j] = tds.get(i).text();
                j++;
            } else {
                for (int ite=0; ite < Integer.parseInt(tds.get(i).attr("colspan")); ite++) {
                    line[j] = tds.get(i).text();
                    j++;
                }
            }
            i++;
        }
    }

    String[] getLine() {
        return line;
    }
}
