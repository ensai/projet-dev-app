package fr.univrennes1.istic.wikipediamatrix.csv;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CsvFile {

    public static List<String[]> get(Elements trs){
        List<String[]> resu = new ArrayList<>();

        Elements tds;
        Line line;
        int nbLignes = 0;
        int nbCol;
        int j = 0;
        for (Element tr : trs) {
            tds = tr.children();
            if (nbLignes == 0) {
                if (tds.size() != 0) {
                    if (resu.size() != 0) {
                        line = new Line(resu.get(0).length, tds);
                        resu.add(line.getLine());
                    } else {
                        if (tds.eachText().size() != new HashSet<>(tds.eachText()).size() && tds.select("td").size() != 0) {
                            nbCol = trs.size();
                            nbLignes = tds.eachText().size();
                            for (int i = 0; i < nbLignes; i++) {
                                resu.add(new String[nbCol]);
                                resu.get(i)[j] = tds.get(i).text();
                            }
                            j++;
                        } else {
                            line = new Line(tds);
                            resu.add(line.getLine());
                        }
                    }
                }
            } else {
                for (int i = 0; i < nbLignes; i++) {
                    resu.get(i)[j] = tds.get(i).text();
                }
                j++;
            }
        }
        return resu;
    }
}
