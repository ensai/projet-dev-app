package fr.univrennes1.istic.wikipediamatrix.csv;

import com.opencsv.CSVWriter;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CsvWriter {

    public static void write(List<String[]> csvFile, String outputFile) throws IOException {
        try (
                Writer writer = Files.newBufferedWriter(Paths.get(outputFile))
        ) {
            CSVWriter csvWriter = new CSVWriter(writer,
                    CSVWriter.DEFAULT_SEPARATOR,
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            csvWriter.writeAll(csvFile);
        }
    }

}
