package fr.univrennes1.istic.wikipediamatrix;

import fr.univrennes1.istic.wikipediamatrix.csv.CsvFile;
import fr.univrennes1.istic.wikipediamatrix.csv.CsvWriter;
import fr.univrennes1.istic.wikipediamatrix.myconnection.MyConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    static String BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/";
    static String BIENVENUE = "Bienvenue !";
    static String CHOIX_URL = "Veuillez entrer une url wikipedia de laquelle extraire les tableaux !";
    static String OUTPUT_FOLDER = "Les fichiers csv sont situés dans le dossier ";
    static String OUTPUT_DIR = "output" + File.separator + "main" + File.separator;

    public static void main( String[] args ) throws IOException {
        System.out.println( BIENVENUE );
        System.out.println( CHOIX_URL );
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String url = br.readLine();
        if (! url.contains("wikipedia.org")) url = BASE_WIKIPEDIA_URL + url;

        Document doc;

        try {
            doc = MyConnection.get(url);
            assert doc != null;
            Elements tables = doc.getElementsByClass("wikitable");
            int i = 1;
            for (Element table : tables) {
                Elements trs = table.select("tr");
                List<String[]> resu = CsvFile.get(trs);
                String csvFileName = mkCSVFileName(url, i);
                CsvWriter.write(resu,OUTPUT_DIR + csvFileName);
                i++;
            }
        } catch (IOException exception) {
            System.out.println("erreur à la connexion : " + url);
        } catch (AssertionError exception) {
            System.out.println("erreur code HTTP, document null : " + url);
        }


        System.out.println( OUTPUT_FOLDER + OUTPUT_DIR );
    }

    private static String mkCSVFileName(String url, int n) {

        return url.split("/")[url.split("/").length - 1].trim() + "-" + n + ".csv";
    }
}
