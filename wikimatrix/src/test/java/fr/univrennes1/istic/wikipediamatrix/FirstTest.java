package fr.univrennes1.istic.wikipediamatrix;

import fr.univrennes1.istic.wikipediamatrix.csv.CsvFile;
import fr.univrennes1.istic.wikipediamatrix.csv.CsvReader;
import fr.univrennes1.istic.wikipediamatrix.csv.CsvWriter;
import fr.univrennes1.istic.wikipediamatrix.myconnection.MyConnection;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FirstTest {

    @Test
    public void testUrlBase() throws Exception {

        String BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/";
        String PAGE_URL = "Comparison_of_Canon_EOS_digital_cameras";

        // HTTP + Parser
        Document doc = MyConnection.get(BASE_WIKIPEDIA_URL + PAGE_URL);

        // Extraction table
        Elements tables = doc.getElementsByClass("wikitable");
        Elements trs = tables.first().select("tr");

        // Conversion CSV
        List<String[]> resu = CsvFile.get(trs);

        // Ecriture dans le fichier
        CsvWriter.write(resu, "./output/html/FirstTest.csv");

    }

    @Test
    public void testReadCsv() throws IOException {

        String BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/";
        String PAGE_URL = "Comparison_of_Canon_EOS_digital_cameras";

        // HTTP + Parser
        Document doc = MyConnection.get(BASE_WIKIPEDIA_URL + PAGE_URL);

        // Extraction table
        Elements tables = doc.getElementsByClass("wikitable");
        Elements trs = tables.first().select("tr");

        // Read du csv
        String PATH = "./output/html/FirstTest.csv";
        List<Map<String, String>> csvRead = CsvReader.read(PATH);

        // Nb de colonnes exact
        int expectedColumn = trs.select("th").first().parent().select("th").size();
        int actualColumn = csvRead.get(0).size();
        assertEquals(expectedColumn, actualColumn);

        // Nb de lignes exact
        int expectedRow = trs.size()-1;
        int actualRow = csvRead.size();
        assertEquals(expectedRow, actualRow);

        // Nom des colonnes
        
		Set<String> expectedNameCol = new HashSet<>(trs.select("th").first().parent().select("th").eachText()) ;
        Set<String> actualNameCol = csvRead.get(0).keySet();
        assertEquals(expectedNameCol, actualNameCol);
    }

}
