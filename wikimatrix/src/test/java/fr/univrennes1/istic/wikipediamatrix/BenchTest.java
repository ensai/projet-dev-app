package fr.univrennes1.istic.wikipediamatrix;

import fr.univrennes1.istic.wikipediamatrix.csv.CsvFile;
import fr.univrennes1.istic.wikipediamatrix.csv.CsvReader;
import fr.univrennes1.istic.wikipediamatrix.csv.CsvWriter;
import fr.univrennes1.istic.wikipediamatrix.myconnection.MyConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BenchTest {
	
	/*
	*  the challenge is to extract as many relevant tables as possible
	*  and save them into CSV files  
	*  from the 300+ Wikipedia URLs given
	*  see below for more details
	**/
	@Test
	public void testBenchExtractors() throws Exception {
		Logger logger = Logger.getLogger("logger");

		String BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/";
		// directory where CSV files are exported (HTML extractor) 
		String outputDirHtml = "output" + File.separator + "html" + File.separator;
		assertTrue(new File(outputDirHtml).isDirectory());

		File file = new File("inputdata" + File.separator + "wikiurls.txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
	    String url;
	    int nurl = 0;
	    while ((url = br.readLine()) != null) {
	        String wurl = BASE_WIKIPEDIA_URL + url;
			logger.log(Level.INFO, "Wikipedia url: " + wurl);

			Document doc = null;

			try {
				doc = MyConnection.get(wurl);
			} catch (IOException exception) {
				logger.log(Level.WARNING, "erreur à la connexion : " + wurl);
			}

			// for exporting to CSV files, we will use mkCSVFileName
			// example: for https://en.wikipedia.org/wiki/Comparison_of_operating_system_kernels
			// the *first* extracted table will be exported to a CSV file called
			// "Comparison_of_operating_system_kernels-1.csv"
//			String csvFileName = mkCSVFileName(url, 1);
//			System.out.println("CSV file name: " + csvFileName);
			// the *second* (if any) will be exported to a CSV file called
			// "Comparison_of_operating_system_kernels-2.csv"

			try {
				assert doc != null;
				Elements tables = doc.getElementsByClass("wikitable");
				int i = 1;
				for (Element table : tables) {
					Elements trs = table.select("tr");

					List<String[]> resu = CsvFile.get(trs);
					String csvFileName = mkCSVFileName(url, i);
					logger.log(Level.INFO, "CSV file name: " + csvFileName);
					CsvWriter.write(resu,outputDirHtml+csvFileName);
					i++;
				}
			} catch (AssertionError exception) {
				logger.log(Level.WARNING, "erreur code HTTP, document null : " + wurl);
			} catch (ArrayIndexOutOfBoundsException exception) {
			logger.warning(exception.toString());
		}



            nurl++;
	    }
	    
	    br.close();	    
	    assertEquals(nurl, 336);

	}

    @Test
    public void testReadCsv() throws Exception {

		Logger logger = Logger.getLogger("logger");

        String BASE_WIKIPEDIA_URL = "https://en.wikipedia.org/wiki/";
        String outputDirHtml = "output" + File.separator + "html" + File.separator;

        File file = new File("inputdata" + File.separator + "wikiurls.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        String url;
        int nurl = 0;
        while ((url = br.readLine()) != null) {
            String wurl = BASE_WIKIPEDIA_URL + url;
            Document doc = null;

            try {
                doc = MyConnection.get(wurl);
            } catch (IOException exception) {
				logger.log(Level.WARNING, "erreur à la connexion : " + wurl);
            }

            try {
				assert doc != null;
				Elements tables = doc.getElementsByClass("wikitable");
                int i = 1;
                for (Element table : tables) {
                    String csvFileName = mkCSVFileName(url, i);
                    logger.log(Level.INFO, "CSV file name: " + csvFileName);
                    try {
                    	testSurUnCsv(table,csvFileName,outputDirHtml);
					} catch (AssertionError exception) {
                    	logger.log(Level.WARNING, exception.toString());
					}
                    i++;
                }
            } catch (AssertionError exception) {
            	logger.log(Level.WARNING, "erreur code HTTP, document null");
            }

            nurl++;
        }

        br.close();
        assertEquals(nurl, 336);

    }

    private void testSurUnCsv(Element table, String csvFileName, String dirName) throws IOException, AssertionError {
        Elements trs = table.select("tr");

        // Read du csv
        List<Map<String, String>> csvRead = CsvReader.read(dirName+csvFileName);


        int expectedColumn;
		int expectedRow;
        // Nb de colonnes et lignes exact
		if (trs.first().children().eachText().size() != new HashSet<>(trs.first().children().eachText()).size() && trs.first().children().select("td").size() != 0) {
			expectedColumn = trs.size();
			expectedRow = trs.first().children().size()-1;
		} else {
			expectedColumn = 0;
			for (int i=0; i < trs.first().children().size(); i++) {
				if (trs.first().children().get(i).attr("colspan").equals("")) {
					expectedColumn++;
				} else {
					expectedColumn = expectedColumn + Integer.parseInt(trs.first().children().get(i).attr("colspan"));
				}
			}
			expectedRow = trs.size()-1;
		}

        int actualColumn = csvRead.get(0).size();
        int actualRow = csvRead.size();

        assertEquals("nombre de colonnes", expectedColumn, actualColumn);
        assertEquals("nombre de lignes", expectedRow, actualRow);

        // Nom des colonnes
		Set<String> expectedNameCol;
		if (trs.first().children().eachText().size() != new HashSet<>(trs.first().children().eachText()).size() && trs.first().children().select("td").size() != 0) {
			List<String> expectedNameColList = new ArrayList<>();
			StringBuilder value;
			int number;
			for (int i=0; i< trs.size();i++) {
				value = new StringBuilder();
				value.append(trs.get(i).children().first().text());
				number = 0;
				for (int j=0; j < trs.first().children().size();j++){
					try {
						if (expectedNameColList.get(j).contains(value.toString())){
							number++;
						}
					} catch (IndexOutOfBoundsException ignored) {

					}
				}
				if (number != 0) {
					value.append(".");
					value.append(number);
				}
				expectedNameColList.add(value.toString());
			}
			expectedNameCol = new HashSet<>(expectedNameColList);
		} else {
			List<String> expectedNameColList = new ArrayList<>();
			StringBuilder value;
			int number;
			for (int i=0; i< trs.first().children().size();i++) {
				value = new StringBuilder();
				value.append(trs.first().children().get(i).text());
				number = 0;
				for (int j=0; j < trs.first().children().size();j++){
					try {
						if (expectedNameColList.get(j).contains(value.toString())){
							number++;
						}
					} catch (IndexOutOfBoundsException ignored) {

					}
				}
				if (trs.first().children().get(i).attr("colspan").equals("")) {
					if (number != 0) {
						value.append(".");
						value.append(number);
					}
					expectedNameColList.add(value.toString());
				} else {
					for (int j=0; j < Integer.parseInt(trs.first().children().get(i).attr("colspan")); j++) {
						if (number != 0) {
							expectedNameColList.add(value.toString() + "." + number);
						} else {
							expectedNameColList.add(value.toString());
						}
						number++;
					}
				}
			}
			expectedNameCol = new HashSet<>(expectedNameColList);
		}
        Set<String> actualNameCol = csvRead.get(0).keySet();
        assertEquals("noms de colonnes",expectedNameCol, actualNameCol);
    }

	private String mkCSVFileName(String url, int n) {
		return url.trim() + "-" + n + ".csv";
	}

}
