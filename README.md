# Wikipedia Matrix

Extracting Wikipedia tables into CSV files (basic skeleton for testing/benchmarking solutions).

This application search for every table in specified URL and convert it to CSV file.

## Run the project

#### Use

Once the git is cloned:
```
cd wikimatrix
mvn clean install
java -cp target/wikimatrix-1.0-SNAPSHOT.jar fr.univrennes1.istic.wikipediamatrix.App
```

#### Test

We provide 300+ URLs on which our application is tested.
To run the test procedure :
```
cd wikimatrix
mvn test
```

## License

#### This project in under GNU GPLv3 Licence (see LICENSE file for more information).

#### This project is based on https://github.com/acherm/wikipediamatrix-bench

## Architecture

#### Class diagram

![Diagramme de Classe](https://gitlab.com/ensai/projet-dev-app/blob/master/class-diagram.svg)
